<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lista Fornitori</title>
</head>
<body>
	Lista Fornitori
	<form action="mostraFormModificaFornitore.do" method="post">
		<table>
			<tr>
				<th>Id</th>
				<th>Nome</th>
				<th>Indirizzo</th>
				<th>Citt�</th>
				<th>Seleziona</th>
				<th></th>
			</tr>
			<c:forEach items="${listaFornitori}" var="f">
				<tr>
					<td>${f.id}</td>
					<td>${f.nome}</td>
					<td>${f.indirizzo}</td>
					<td>${f.citta}</td>
					<td><input type="radio" name="id" value="${f.id}"></td>
					<td>
						<form action="eliminaFornitore.do" method="post">
							<input type="hidden" name="id" value="${f.id}">
							<input type="submit" value="Elimina">
						</form>
					</td>
				</tr>
			</c:forEach>
		</table>
		<input type="submit" value="Modifica"><br>
	</form>
	<a href="mostraFormModificaFornitore.do">Inserisci nuovo fornitore</a>
</body>
</html>