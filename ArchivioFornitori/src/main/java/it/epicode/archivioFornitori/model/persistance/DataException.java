package it.epicode.archivioFornitori.model.persistance;

public class DataException extends Exception{

	public DataException(String message, Throwable cause) {
		super(message, cause);
		
	}
	
}
