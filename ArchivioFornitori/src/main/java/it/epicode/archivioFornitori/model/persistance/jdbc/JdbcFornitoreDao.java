package it.epicode.archivioFornitori.model.persistance.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import it.epicode.archivioFornitori.model.entities.Fornitore;
import it.epicode.archivioFornitori.model.persistance.DataException;
import it.epicode.archivioFornitori.model.persistance.FornitoreDao;

public class JdbcFornitoreDao implements FornitoreDao{

	public static final String SELECT_ALL_FORNITORI = "select id,nome,indirizzo,citta from negozio.fornitore order by id;";
	public static final String FIND_FORNITORE_BY_ID = "select id,nome,indirizzo,citta from negozio.fornitore where id = ?";
	public static final String SAVE_FORNITORE = "insert into negozio.fornitore (nome, indirizzo, citta) values (?,?,?);";
	public static final String EDIT_FORNITORE = "update negozio.fornitore set nome=?, indirizzo=?, citta=? where id=?;";
	public static final String DELETE_FORNITORE = "delete from negozio.fornitore where id=?;";
	
	@Override
	public List<Fornitore> leggiFornitori() throws DataException {
		try (Connection con = ConnectionUtils.creaConnessione();
			 Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(SELECT_ALL_FORNITORI)) {
			
			List<Fornitore> fornitori = new ArrayList<>();
			while(rs.next()) {
				Fornitore f = new Fornitore(rs.getLong("id"), rs.getString("nome"), rs.getString("indirizzo"), rs.getString("citta"));
				fornitori.add(f);
			}
			return fornitori;
			
		} catch (SQLException e) {
			DataException de = new DataException(e.getMessage(), e);
			throw de;
		}
	}

	@Override
	public Optional<Fornitore> trovaPerId(long id) throws DataException {
		try(Connection con = ConnectionUtils.creaConnessione();
			PreparedStatement ps = con.prepareStatement(FIND_FORNITORE_BY_ID)){
			ps.setLong(1, id);
			try(ResultSet rs = ps.executeQuery()) {
				if(rs.next()) {
					Fornitore f = new Fornitore(rs.getLong("id"), rs.getString("nome"), rs.getString("indirizzo"), rs.getString("citta"));
					return Optional.of(f);
				}
				return Optional.empty();
			}
		} catch (SQLException e) {
			throw new DataException(e.getMessage(), e);
		}	
	}

	@Override
	public Fornitore salva(Fornitore f) throws DataException {
		try(Connection con = ConnectionUtils.creaConnessione();
			PreparedStatement ps = con.prepareStatement(SAVE_FORNITORE, Statement.RETURN_GENERATED_KEYS)) {
			ps.setString(1, f.getNome());
			ps.setString(2, f.getIndirizzo());
			ps.setString(3, f.getCitta());
			ps.executeUpdate();
			ResultSet keys= ps.getGeneratedKeys();
			if(keys.next()) {
				f.setId(keys.getLong(1));
			}
			return f;
		} catch (SQLException e) {
			throw new DataException(e.getMessage(), e);
		}
	}

	@Override
	public boolean modifica(Fornitore f) throws DataException {
		try(Connection con = ConnectionUtils.creaConnessione();
			PreparedStatement ps = con.prepareStatement(EDIT_FORNITORE)){
			ps.setString(1, f.getNome());
			ps.setString(2, f.getIndirizzo());
			ps.setString(3, f.getCitta());
			ps.setLong(4, f.getId());
			int rownum = ps.executeUpdate();
			
//			if(rownum == 1) {
//				return true;
//			}else return false;
			
			return rownum == 1;
		} catch (SQLException e) {
			throw new DataException(e.getMessage(), e);
		}
	}

	@Override
	public boolean elimina(long id) throws DataException {
		try(Connection con = ConnectionUtils.creaConnessione();
			PreparedStatement ps = con.prepareStatement(DELETE_FORNITORE)){
			ps.setLong(1, id);
			int rownum = ps.executeUpdate();
			
			return rownum == 1;
		} catch (SQLException e) {
			throw new DataException(e.getMessage(), e);
		}
		
	}

}
