package it.epicode.archivioFornitori.model.persistance;

import java.util.List;
import java.util.Optional;

import it.epicode.archivioFornitori.model.entities.Fornitore;

public interface FornitoreDao {
	List<Fornitore> leggiFornitori() throws DataException;
	Optional<Fornitore> trovaPerId(long id) throws DataException;
	Fornitore salva(Fornitore f) throws DataException;
	boolean modifica(Fornitore f) throws DataException;
	boolean elimina(long id) throws DataException;
}
