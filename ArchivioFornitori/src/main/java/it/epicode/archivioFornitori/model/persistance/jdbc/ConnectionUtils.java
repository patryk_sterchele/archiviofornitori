package it.epicode.archivioFornitori.model.persistance.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtils {
	
	public static final String HOST = "localhost";
	public static final int PORT = 5432;
	public static final String DB_NAME = "ProgettoS4";
	public static final String SCHEMA = "negozio";
	public static final String UTENTE = "postgres";
	public static final String PASSWORD = "lison30026";
	
	public static Connection creaConnessione() throws SQLException {
		String url = String.format("jdbc:postgresql://%s:%d/%s?currentSchema=%s&user=%s&password=%s", HOST,PORT,
				DB_NAME,SCHEMA,UTENTE,PASSWORD);
		return DriverManager.getConnection(url);
	}
}
