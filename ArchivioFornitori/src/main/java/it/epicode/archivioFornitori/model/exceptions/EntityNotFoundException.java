package it.epicode.archivioFornitori.model.exceptions;

public class EntityNotFoundException extends Exception{

	public EntityNotFoundException(String message) {
		super(message);
	}
	
}
