package it.epicode.archivioFornitori.controller.actions;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.epicode.archivioFornitori.controller.Action;
import it.epicode.archivioFornitori.model.entities.Fornitore;
import it.epicode.archivioFornitori.model.persistance.DataException;
import it.epicode.archivioFornitori.model.persistance.FornitoreDao;

public class ListaFornitoriAction implements Action{
	
	private FornitoreDao fornitoreDao;

	public ListaFornitoriAction(FornitoreDao fornitoreDao) {
		this.fornitoreDao = fornitoreDao;
	}
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws DataException {
		List<Fornitore> result = fornitoreDao.leggiFornitori();
		request.setAttribute("listaFornitori", result);
		return "listaFornitori.jsp";
	}

}
