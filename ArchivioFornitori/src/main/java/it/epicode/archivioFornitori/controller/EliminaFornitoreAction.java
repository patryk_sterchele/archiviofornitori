package it.epicode.archivioFornitori.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.epicode.archivioFornitori.model.entities.Fornitore;
import it.epicode.archivioFornitori.model.exceptions.EntityNotFoundException;
import it.epicode.archivioFornitori.model.persistance.DataException;
import it.epicode.archivioFornitori.model.persistance.FornitoreDao;

public class EliminaFornitoreAction implements Action {
	
	public FornitoreDao fornitoreDao;
	
	public EliminaFornitoreAction(FornitoreDao fornitoreDao) {
		this.fornitoreDao = fornitoreDao;
	}



	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws DataException, EntityNotFoundException {
		long id = Long.parseLong(request.getParameter("id"));
		boolean deleted = fornitoreDao.elimina(id);
		if(deleted) {
			List<Fornitore> result = fornitoreDao.leggiFornitori();
			request.setAttribute("listaFornitori", result);
			return "listaFornitori.jsp";
		}
		throw new EntityNotFoundException("Non posso cancellare un entity che non esiste id: " + id);
	}

}
