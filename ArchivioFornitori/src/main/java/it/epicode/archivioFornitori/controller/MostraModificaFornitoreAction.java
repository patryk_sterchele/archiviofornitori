package it.epicode.archivioFornitori.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.epicode.archivioFornitori.model.entities.Fornitore;
import it.epicode.archivioFornitori.model.exceptions.EntityNotFoundException;
import it.epicode.archivioFornitori.model.persistance.DataException;
import it.epicode.archivioFornitori.model.persistance.FornitoreDao;

public class MostraModificaFornitoreAction implements Action {

	private FornitoreDao fornitoreDao;
	
	public MostraModificaFornitoreAction(FornitoreDao fornitoreDao) {
		this.fornitoreDao = fornitoreDao;
	}


	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws DataException, EntityNotFoundException {
		String idString = request.getParameter("id");
		if(idString == null || idString.isEmpty()) {
			return "aggiungiModificaFornitore.jsp";
		}
		Long id = Long.parseLong(idString);
		Optional<Fornitore> of = fornitoreDao.trovaPerId(id);
		if(of.isPresent()) {
			Fornitore f = of.get();
			request.setAttribute("fornitore", f);
			return "aggiungiModificaFornitore.jsp";
		}else {
			throw new EntityNotFoundException("Fornitore con id " + id + " non esiste");
		}
	}

}
