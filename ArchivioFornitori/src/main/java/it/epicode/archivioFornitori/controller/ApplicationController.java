package it.epicode.archivioFornitori.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.epicode.archivioFornitori.controller.actions.ListaFornitoriAction;
import it.epicode.archivioFornitori.model.exceptions.EntityNotFoundException;
import it.epicode.archivioFornitori.model.persistance.DataException;
import it.epicode.archivioFornitori.model.persistance.jdbc.JdbcFornitoreDao;


@WebServlet("*.do")
public class ApplicationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	static {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Non � possbile caricare il driver");
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String path = request.getServletPath();
		String actionString = path.substring(path.lastIndexOf('/')+1, path.length() - 3);
		Action action = null;
		switch (actionString) {
		case "listaFornitori":
			action = new ListaFornitoriAction(new JdbcFornitoreDao());
			break;
		case "mostraFormModificaFornitore":
			 action = new MostraModificaFornitoreAction(new JdbcFornitoreDao());
			 break;
		case "inserisciOModificaFornitore":
			action = new InserisciOModificaFornitore(new JdbcFornitoreDao());
			break;
		case "eliminaFornitore":
			action = new EliminaFornitoreAction(new JdbcFornitoreDao());
			break;
		default:
			break;
		}
		String view = null;
		try {
			view = action.execute(request, response);
		} catch (DataException e) {
			e.printStackTrace();
			view = "internalError.jsp";
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
			view = "clientError.jsp";
			request.setAttribute("exception", e);
		}
		RequestDispatcher rd = request.getRequestDispatcher("jsp/"+view);
		rd.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
