package it.epicode.archivioFornitori.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.epicode.archivioFornitori.model.entities.Fornitore;
import it.epicode.archivioFornitori.model.exceptions.EntityNotFoundException;
import it.epicode.archivioFornitori.model.persistance.DataException;
import it.epicode.archivioFornitori.model.persistance.FornitoreDao;

public class InserisciOModificaFornitore implements Action {
	
	FornitoreDao fornitoreDao;

	public InserisciOModificaFornitore(FornitoreDao fornitoreDao) {
		this.fornitoreDao = fornitoreDao;
	}



	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws DataException, EntityNotFoundException {
		String idString = request.getParameter("id");
		Fornitore f = new Fornitore(idString == null || idString.isEmpty()? 0 : Long.parseLong(idString),
						request.getParameter("nome"),
						request.getParameter("indirizzo"),
						request.getParameter("citta"));
		if(f.getId() == 0) {
			fornitoreDao.salva(f);
		}else {
			fornitoreDao.modifica(f);
		}
		List<Fornitore> result = fornitoreDao.leggiFornitori();
		request.setAttribute("listaFornitori", result);
		return "listaFornitori.jsp";
	}

}
