package it.epicode.archivioFornitori.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.epicode.archivioFornitori.model.exceptions.EntityNotFoundException;
import it.epicode.archivioFornitori.model.persistance.DataException;

public interface Action {
	String execute(HttpServletRequest request, HttpServletResponse response) throws DataException, EntityNotFoundException;
}
